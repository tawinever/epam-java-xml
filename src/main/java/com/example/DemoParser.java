package com.example;

import com.example.domain.VoucherInfo;
import com.example.xml.XMLParser;
import com.example.xml.parser.DOMParser;
import com.example.xml.parser.SAXParser;
import com.example.xml.parser.StAXParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DemoParser {
    private static final Logger logger = LogManager.getLogger(DemoParser.class);

    public static void main(String[] args) {
        XMLParser xmlParser = new XMLParser.Builder()
//                .withParser(new DOMParser())
//                .withParser(new SAXParser())
                .withParser(new StAXParser())
                .withFilePath("touristVouchers.xml").build();
        for(VoucherInfo voucherInfo : xmlParser.parse()) {
            logger.info("Voucher id: " + voucherInfo.getVoucherId() + ", Price: " + voucherInfo.getVoucherCost());
        }
    }
}
