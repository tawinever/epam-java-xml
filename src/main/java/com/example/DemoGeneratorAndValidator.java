package com.example;

import com.example.xml.XMLGenerator;
import com.example.xml.XMLValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

public class DemoGeneratorAndValidator {
    private static final Logger logger = LogManager.getLogger(DemoGeneratorAndValidator.class);

    public static void main(String[] args) {
        /*
        *  Generate with scheme only if scheme doesn't exist
        *  Scheme was edited manually (pattern, cardinality etc.)
        * */
        XMLGenerator.generateFiles(false);
        try {
            if (XMLValidator.isValid("touristVouchers.xml", "TouristVouchers.xsd"))
            {
                logger.info("Valid");
            }
        } catch (SAXException e) {
            logger.info("Not Valid");
            e.printStackTrace();
        }
    }
}
