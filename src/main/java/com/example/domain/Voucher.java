package com.example.domain;

import com.example.domain.enums.TransportType;
import com.example.domain.enums.VoucherType;
import com.example.domain.hotelCharacteristic.HotelCharacteristic;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlType(propOrder = {"voucherType", "country", "numberOfDaysAndNights", "transport", "hotelCharacteristic", "cost"})
@XmlAccessorType(XmlAccessType.FIELD)
public class Voucher {
    @XmlAttribute(required = true)
    @XmlID
    private String id;

    @XmlAttribute
    private String paymentMethod;

    @XmlElement(required = true)
    private VoucherType voucherType;

    @XmlElement(required = true)
    private String country;

    @XmlList
    @XmlElement(required = true)
    private List<Integer> numberOfDaysAndNights;

    @XmlElement(required = true)
    private TransportType transport;
    private List<? extends HotelCharacteristic> hotelCharacteristic;

    @XmlElement(required = true)
    private Cost cost;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public VoucherType getVoucherType() {
        return voucherType;
    }

    public void setVoucherType(VoucherType voucherType) {
        this.voucherType = voucherType;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setNumberOfDaysAndNights(List<Integer> numberOfDaysAndNights) {
        this.numberOfDaysAndNights = numberOfDaysAndNights;
    }

    public TransportType getTransport() {
        return transport;
    }

    public void setTransport(TransportType transport) {
        this.transport = transport;
    }

    public List<? extends HotelCharacteristic> getHotelCharacteristic() {
        return hotelCharacteristic;
    }

    public void setHotelCharacteristic(List<? extends HotelCharacteristic> hotelCharacteristic) {
        this.hotelCharacteristic = hotelCharacteristic;
    }

    public Cost getCost() {
        return cost;
    }

    public void setCost(Cost cost) {
        this.cost = cost;
    }
}
