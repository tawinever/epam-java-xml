package com.example.domain.enums;

public enum TransportType {
    PLANE,
    TRAIN,
    CAR,
    LINER;
}
