package com.example.domain.enums;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;

@XmlEnum(Integer.class)
public enum RoomType {
    @XmlEnumValue("1") ONE_BEDROOM,
    @XmlEnumValue("2") TWO_BEDROOMS,
    @XmlEnumValue("3") THREE_BEDROOMS
}
