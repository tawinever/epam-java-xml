package com.example.domain.enums;

public enum VoucherType {
    WEEKEND,
    TOUR,
    VACATION;
}
