package com.example.domain.enums;

import javax.xml.bind.annotation.XmlEnum;

@XmlEnum(String.class)
public enum MealType {
    HB,
    BB,
    AL;
}
