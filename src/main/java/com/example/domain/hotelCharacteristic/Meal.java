package com.example.domain.hotelCharacteristic;

import com.example.domain.enums.MealType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlValue;

@XmlAccessorType(XmlAccessType.NONE)
public class Meal extends HotelCharacteristic {

    @XmlValue
    private MealType mealType;


    public Meal(MealType mealType) {
        this.mealType = mealType;
    }
}
