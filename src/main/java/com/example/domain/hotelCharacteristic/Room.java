package com.example.domain.hotelCharacteristic;

import com.example.domain.enums.RoomType;
import com.example.domain.hotelCharacteristic.HotelCharacteristic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlValue;

@XmlAccessorType(XmlAccessType.NONE)
public class Room extends HotelCharacteristic {
    @XmlValue
    private RoomType room;

    public Room(RoomType room) {
        this.room = room;
    }
}
