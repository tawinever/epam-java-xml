package com.example.domain.hotelCharacteristic;

import com.example.domain.hotelCharacteristic.HotelCharacteristic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

@XmlAccessorType(XmlAccessType.NONE)
public class ItemCheck extends HotelCharacteristic {
    @XmlAttribute
    private String item;

    @XmlValue
    private Boolean isExist;

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public Boolean getExist() {
        return isExist;
    }

    public void setExist(Boolean exist) {
        isExist = exist;
    }

    public ItemCheck(String item, Boolean isExist) {
        this.item = item;
        this.isExist = isExist;
    }
}
