package com.example.domain.hotelCharacteristic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlValue;

@XmlAccessorType(XmlAccessType.NONE)
public class StarRank extends HotelCharacteristic {
    @XmlValue
    private int rank;

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public StarRank(int rank) {
        this.rank = rank;
    }
}
