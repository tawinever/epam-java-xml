package com.example.domain.hotelCharacteristic;

import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlTransient;

@XmlSeeAlso({StarRank.class, Meal.class, ItemCheck.class, Room.class})
@XmlTransient
public class HotelCharacteristic {
}
