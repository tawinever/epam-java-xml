package com.example.domain;

public class VoucherInfo {
    private String voucherId;
    private Double voucherCost;

    public String getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(String voucherId) {
        this.voucherId = voucherId;
    }

    public Double getVoucherCost() {
        return voucherCost;
    }

    public void setVoucherCost(Double voucherCost) {
        this.voucherCost = voucherCost;
    }

    public VoucherInfo() {
    }

    public VoucherInfo(String voucherId, Double voucherCost) {
        this.voucherId = voucherId;
        this.voucherCost = voucherCost;
    }
}
