package com.example.domain;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement()
public class TouristVouchers {

    private List<Voucher> vouchers;

    public TouristVouchers() {
    }

    public TouristVouchers(List<Voucher> vouchers) {
        this.vouchers = vouchers;
    }

    public List<Voucher> getVoucher() {
        return vouchers;
    }

    public void setVoucher(List<Voucher> voucher) {
        this.vouchers = voucher;
    }

}
