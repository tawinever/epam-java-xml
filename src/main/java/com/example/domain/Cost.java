package com.example.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

@XmlAccessorType(XmlAccessType.FIELD)
public class Cost {
    private double total;

    @XmlAttribute
    private String currency;
    private String description;

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCurrency() {
        if (currency == null)
            return "USD";
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Cost(double total, String description) {
        this.total = total;
        this.description = description;
    }
}
