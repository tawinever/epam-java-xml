package com.example.helper;

import java.util.Random;

public class RandomGenerator {
    private static final String ALPHA_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public static int getInt(int min, int max) {
        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

    public static String getString(int length) {
        StringBuilder builder = new StringBuilder();
        while (length != 0) {
            length--;
            int character = (int)(Math.random()*ALPHA_STRING.length());
            builder.append(ALPHA_STRING.charAt(character));
        }
        return builder.toString();
    }
}
