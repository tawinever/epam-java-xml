package com.example.xml;

import com.example.domain.VoucherInfo;
import com.example.xml.parser.XMLParsable;

import java.util.List;

public class XMLParser {
    private XMLParsable parser;
    private String filePath;

    public List<VoucherInfo> parse() {
        return parser.getVoucherInfoList(filePath);
    }

    private XMLParser(Builder builder) {
        this.parser = builder.parser;
        this.filePath = builder.filePath;
    }

    public static class Builder {
        private XMLParsable parser;
        private String filePath;

        public Builder withParser(XMLParsable parser) {
            this.parser = parser;
            return this;
        }

        public Builder withFilePath(String filePath) {
            this.filePath = filePath;
            return this;
        }

        public XMLParser build() {
            return new XMLParser(this);
        }
    }
}
