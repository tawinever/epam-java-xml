package com.example.xml.parser;

import com.example.domain.VoucherInfo;

import java.util.List;

public interface XMLParsable {
    public List<VoucherInfo> getVoucherInfoList(String filePath);
}
