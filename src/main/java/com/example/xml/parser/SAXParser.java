package com.example.xml.parser;

import com.example.domain.VoucherInfo;
import com.example.xml.parser.handler.SAXHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

public class SAXParser implements XMLParsable {
    private static final Logger logger = LogManager.getLogger(SAXParser.class);

    @Override
    public List<VoucherInfo> getVoucherInfoList(String filePath) {
        try {
            javax.xml.parsers.SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
            SAXHandler saxHandler = new SAXHandler();
            parser.parse(filePath, saxHandler);
            return saxHandler.getVoucherInfoList();
        } catch (ParserConfigurationException | IOException | SAXException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            return Collections.emptyList();
        }
    }
}
