package com.example.xml.parser;

import com.example.domain.VoucherInfo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class DOMParser implements XMLParsable {
    private static final Logger logger = LogManager.getLogger(DOMParser.class);

    @Override
    public List<VoucherInfo> getVoucherInfoList(String filePath) {
        List<VoucherInfo> voucherInfoList = new ArrayList<>();
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(new File(filePath));
            doc.getDocumentElement().normalize();
            logger.info("Parsing started.");
            NodeList vouchers = doc.getElementsByTagName("voucher");
            for (int temp = 0; temp < vouchers.getLength(); temp++) {
                Node node = vouchers.item(temp);

                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    Element cost = (Element) element.getElementsByTagName("cost").item(0);
                    voucherInfoList.add(new VoucherInfo(
                            element.getAttribute("id"),
                            Double.parseDouble(cost.getElementsByTagName("total").item(0).getTextContent())));
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }

        return voucherInfoList;
    }
}

