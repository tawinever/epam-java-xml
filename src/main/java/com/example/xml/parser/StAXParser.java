package com.example.xml.parser;

import com.example.domain.VoucherInfo;
import com.example.xml.parser.handler.StAXHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Collections;
import java.util.List;

public class StAXParser implements XMLParsable {
    private static final Logger logger = LogManager.getLogger(StAXParser.class);

    @Override
    public List<VoucherInfo> getVoucherInfoList(String filePath) {
        try {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            XMLStreamReader reader = inputFactory.createXMLStreamReader(new FileInputStream(filePath));
            return StAXHandler.process(reader);
        } catch (XMLStreamException | FileNotFoundException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            return Collections.emptyList();
        }
    }
}
