package com.example.xml.parser.handler;

import com.example.domain.VoucherInfo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class StAXHandler {
    private static final Logger logger = LogManager.getLogger(StAXHandler.class);

    public static List<VoucherInfo> process(XMLStreamReader reader) throws XMLStreamException {
        List<VoucherInfo> voucherInfoList = new ArrayList<>();
        VoucherInfo voucherInfo = null;
        String tagName = null;
        logger.info("Parsing started.");
        while (reader.hasNext()) {
            int type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    tagName = reader.getLocalName();
                    if (tagName.equals("voucher")) {
                        voucherInfo = new VoucherInfo();
                        voucherInfo.setVoucherId(reader.getAttributeValue(null, "id"));
                    }
                    break;
                case XMLStreamConstants.CHARACTERS:
                    String text = reader.getText().trim();
                    if (text.isEmpty()) {
                        break;
                    }
                    if (Objects.equals(tagName, "total")) {
                        if (voucherInfo != null) {
                            voucherInfo.setVoucherCost(Double.parseDouble(text));
                        }
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    if (Objects.equals(reader.getLocalName(), "voucher")) {
                        voucherInfoList.add(voucherInfo);
                    }
                    break;
                default:
                    break;

            }
        }
        logger.info("Parsing ended.");
        return voucherInfoList;
    }
}
