package com.example.xml.parser.handler;

import com.example.domain.VoucherInfo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class SAXHandler extends DefaultHandler {
    private static final Logger logger = LogManager.getLogger(SAXHandler.class);

    private List<VoucherInfo> voucherInfoList = new ArrayList<>();
    private VoucherInfo voucherInfo;
    private StringBuilder text;

    public List<VoucherInfo> getVoucherInfoList() {
        return voucherInfoList;
    }

    public void startDocument() throws SAXException {
        logger.info("Parsing started.");
    }

    public void endDocument() throws SAXException {
        logger.info("Parsing ended.");
    }

    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        text = new StringBuilder();
        if (qName.equals("voucher")){
            voucherInfo = new VoucherInfo();
            voucherInfo.setVoucherId(attributes.getValue("id"));
        }
    }

    public void characters(char[] buffer, int start, int length) {
        text.append(buffer, start, length);
    }

    public void endElement(String uri, String localName, String qName) throws SAXException {
        switch(qName){
            case "voucher":
                voucherInfoList.add(voucherInfo);
                break;
            case "total":
                voucherInfo.setVoucherCost(Double.parseDouble(text.toString()));
                break;
            default:
                break;
        }
    }

    public void warning(SAXParseException exception) {
        logger.warn("WARNING: line " + exception.getLineNumber() + ": "	+ exception.getMessage());
    }

    public void error(SAXParseException exception) {
        logger.error("ERROR: line " + exception.getLineNumber() + ": "	+ exception.getMessage());
    }

    public void fatalError(SAXParseException exception) throws SAXException {
        logger.error("FATAL: line " + exception.getLineNumber() + ": " + exception.getMessage());
        throw (exception);
    }
}
