package com.example.xml;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;

public class XMLValidator {
    private static final Logger logger = LogManager.getLogger(XMLValidator.class);

    public static boolean isValid(String xmlPath, String schemePath) throws SAXException {
        SchemaFactory factory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");

        File schemaLocation = new File(schemePath);
        Schema schema = factory.newSchema(schemaLocation);
        Validator validator = schema.newValidator();
        Source source = new StreamSource(xmlPath);
        try {
            validator.validate(source);
            logger.info(xmlPath + " is valid.");
            return true;
        } catch (SAXException | IOException ex) {
            logger.info(xmlPath + " is not valid because ");
            logger.info(ex.getMessage());
            return false;
        }
    }
}
