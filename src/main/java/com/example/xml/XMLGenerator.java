package com.example.xml;

import com.example.domain.Cost;
import com.example.domain.TouristVouchers;
import com.example.domain.Voucher;
import com.example.domain.enums.MealType;
import com.example.domain.enums.RoomType;
import com.example.domain.enums.TransportType;
import com.example.domain.enums.VoucherType;
import com.example.domain.hotelCharacteristic.ItemCheck;
import com.example.domain.hotelCharacteristic.Meal;
import com.example.domain.hotelCharacteristic.Room;
import com.example.domain.hotelCharacteristic.StarRank;
import com.example.helper.RandomGenerator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.SchemaOutputResolver;
import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class XMLGenerator {
    private static final Logger logger = LogManager.getLogger(XMLGenerator.class);

    private static class MySchemaOutputResolver extends SchemaOutputResolver {
        private String fileName = null;

        public MySchemaOutputResolver(String fileName) {
            this.fileName = fileName;
        }

        public Result createOutput(String namespaceURI, String suggestedFileName) throws IOException {
            String filePath;
            if (this.fileName == null) {
                filePath = suggestedFileName;
            } else {
                filePath = this.fileName + ".xsd";
            }
            File file = new File(filePath);
            StreamResult result = new StreamResult(file);
            result.setSystemId(file.toURI().toURL().toString());
            return result;
        }
    }

    public static void generateFiles(Boolean withScheme) {
        try {
            logger.info("Starting generating file");
            JAXBContext context = JAXBContext.newInstance(TouristVouchers.class);

            if (withScheme) {
                SchemaOutputResolver sor = new MySchemaOutputResolver(TouristVouchers.class.getSimpleName());
                context.generateSchema(sor);
            }

            Marshaller m = context.createMarshaller();

            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            m.setProperty(Marshaller.JAXB_NO_NAMESPACE_SCHEMA_LOCATION, TouristVouchers.class.getSimpleName() + ".xsd");

            TouristVouchers tv = new TouristVouchers(createVouchers(20));

            m.marshal(tv, new FileOutputStream("touristVouchers.xml"));
            logger.info("XML file generated");
        } catch (JAXBException | IOException e) {
            e.printStackTrace();
        }
    }

    private static List<Voucher> createVouchers(int quantity) {
        List<Voucher> vouchers = new ArrayList<>();
        for (int i = 0; i < quantity; i++) {
            Voucher voucher = new Voucher();
            voucher.setId("KZ" + i);
            voucher.setVoucherType(VoucherType.values()[RandomGenerator.getInt(0, VoucherType.values().length - 1)]);
            voucher.setCountry(RandomGenerator.getString(3));
            voucher.setCost(new Cost(RandomGenerator.getInt(30,50) * 10000.0, "Lorem Ipsum"));
            voucher.setHotelCharacteristic(Arrays.asList(
                    new StarRank(RandomGenerator.getInt(1,5)),
                    new ItemCheck("Meal", Boolean.TRUE),
                    new Meal(MealType.AL),
                    new Room(RoomType.TWO_BEDROOMS),
                    new ItemCheck("TV", Boolean.TRUE),
                    new ItemCheck("AirConditioner", Boolean.TRUE)
            ));
            int days = RandomGenerator.getInt(7,14);
            voucher.setNumberOfDaysAndNights(Arrays.asList(days, days + RandomGenerator.getInt(0, 1)));
            voucher.setTransport(TransportType.values()[RandomGenerator.getInt(0, TransportType.values().length - 1)]);
            vouchers.add(voucher);
        }

        return vouchers;
    }
}
